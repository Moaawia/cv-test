import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
posts:any[]=[];
url: string ='https://jsonplaceholder.typicode.com/posts';
  constructor(private http:Http) { }

  ngOnInit(): void {
    this.http.get(this.url).subscribe(response => {
      this.posts = response.json();
    })
  }
  addPost(input:any){
    let post={title:input.value,id:''}
    this.http.post(this.url, JSON.stringify(post)).subscribe(response => { 
    this.posts.splice(0,0,post)
     });
    
  }

}
