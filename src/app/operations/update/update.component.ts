import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  posts: any[] = [];
  url: string = 'https://jsonplaceholder.typicode.com/posts';
  constructor(private http: Http) { }

  ngOnInit(): void {
    this.http.get(this.url).subscribe(response => {
      this.posts = response.json();
    })
  }
  updatePost(post: any,inputTitle:any) {
    let updatePost={title:inputTitle,id:post.id}
    this.http.put(this.url + '/' + post.id,post.id,JSON.stringify(updatePost) ).subscribe(response => {
      let index = this.posts.indexOf(post);
      this.posts[index] = updatePost;
    })
  }

}
