import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  posts: any[] = [];
  url: string = 'https://jsonplaceholder.typicode.com/posts';
  constructor(private http: Http) { }

  ngOnInit(): void {
    this.http.get(this.url).subscribe(response => {
      this.posts = response.json();
    })
  }
  deletePost(post: any) {
    let index = this.posts.indexOf(post);
    this.http.delete(this.url + '/' + post.id).subscribe(response => { (this.posts.splice(index, 1)); })
  }
}
