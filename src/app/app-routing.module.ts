import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './operations/add/add.component';
import { DeleteComponent } from './operations/delete/delete.component';
import { UpdateComponent } from './operations/update/update.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'operations/update',component:UpdateComponent},
  {path:'operations/delete',component:DeleteComponent},
  {path:'operations/add',component:AddComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
